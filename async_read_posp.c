//
// Created by json on 2022/11/16.
//

#include "async_read_posp.h"
#include "log.h"

#include <assert.h>
#include <string.h>
#include "stdlib.h"

struct { char *app; int len; READ_CALLBACK cb; } CTX[1024];

#define STX 0x02
#define ETX 0x03

#define READ_POSP_REQ_ERR -101

void
bcd_to_asc(unsigned char *dst, unsigned char *src, unsigned int len) {
    static unsigned char bcd2ascii[16] =
            {'0', '1', '2', '3', '4', '5', '6',
             '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    unsigned int i = 0;
    for (i = 0; i < len; ++i) {
        *dst++ = bcd2ascii[(src[i]) >> 4];
        *dst++ = bcd2ascii[(src[i]) & 0x0f];
    }
}

void body_read_cb(int fd, int err, char *dst, int size_transferred) {
    if (err) {
        CTX[fd].cb(fd, err, CTX[fd].app, 0);
        return;
    }
    char *p = dst;
    assert(p[size_transferred-2] == ETX);
    CTX[fd].len += size_transferred;
    CTX[fd].cb(fd, 0, CTX[fd].app, CTX[fd].len);
}

void len_read_cb(int fd, int err, char *dst, int size_transferred) {
    if (err) {
        CTX[fd].cb(fd, err, CTX[fd].app, 0);
        return;
    }
    char *p = dst;
    if (*p != STX) {
        CTX[fd].cb(fd, READ_POSP_REQ_ERR, CTX[fd].app, 0);
        return;
    }
    p++;
    char tmp[8] = { 0 };
    bcd_to_asc(tmp, p, 2);
    p += 2;
    int len = atoi(tmp);
    CTX[fd].len += size_transferred;
    async_read(fd, len + 2, p, body_read_cb);
}


boolean async_read_posp_req(int fd, char *dst, READ_CALLBACK cb) {
    CTX[fd].app = dst;
    CTX[fd].cb = cb;
    CTX[fd].len = 0;
    return async_read(fd, 1 + 2, CTX[fd].app, len_read_cb);
}


boolean async_read_posp(int fd, buffer_t b, POSP_READ_CALLBACK cb) {

}

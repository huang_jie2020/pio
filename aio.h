//
// Created by json on 2022/11/15.
//

#ifndef AIO_AIO_H
#define AIO_AIO_H

#include "type_def.h"

int     tcp_socket();
boolean tcp_socket_enable_nonblock(int fd);
boolean socket_bind_listen(int fd, short port);

boolean aio_init();
void    aio_free();
void    aio_sync();
boolean aio_in_io_thread();

// async read
boolean async_read      (int fd, int size_to_read, char *dst, READ_CALLBACK cb, size_t expire);
boolean async_read_some (int fd, int size_to_read, char *dst, READ_CALLBACK cb, size_t expire);
boolean async_accept    (int fd, ACCEPT_CALLBACK cb, size_t expire);

// async write
boolean async_write     (int fd, int size_to_write, const char *src, WRITE_CALLBACK cb, size_t timeout);
boolean async_connect   (int fd, const char *ip, unsigned short port, CONNECT_CALLBACK cb, size_t timeout);

// cancel the current blocking async read
boolean aio_cancel_read(int fd);
boolean aio_cancel_read_with_err(int fd, int err);
// cancel the current blocking async write
boolean aio_cancel_write(int fd);
boolean aio_cancel_write_with_err(int fd, int err);

const char* aio_err_desc(int err);


#endif //AIO_AIO_H

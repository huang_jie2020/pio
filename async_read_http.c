//
// Created by json on 22-11-24.
//

#include "async_read_http.h"
#include "aio.h"
#include "http_parser.h"
#include "vector.h"


#define CNT_PREFER  1024

typedef struct st_http_ctx {
    http_parser parser;
    http_parser_settings settings;

    buffer_t buffer;
    size_t len_parsed;

    boolean complete;
    HTTP_READ_CALLBACK cb;
} *http_ctx_t;


static __thread struct {
#define MAX_CAPACITY        1024000
#define INITIAL_CAPACITY    512

    http_ctx_t      d;
    size_t          c;
} HTTP_CTX;

static boolean _read_http_req(int fd);
static void _read_cb(int fd, int err, char *dst, size_t size_transferred);

static int on_message_begin(http_parser *p) {
    http_ctx_t ctx = p->data;
    ctx->complete = FALSE;
    return 0;
}

static int on_message_complete(http_parser *p) {
    http_ctx_t ctx = p->data;
    ctx->complete = TRUE;
    return 0;
}

static int on_headers_complete(http_parser *p) {
    return 0;
}

boolean http_module_init() {
    vec_init(&HTTP_CTX);
    return TRUE;
}

boolean http_module_free() {
    vec_free(&HTTP_CTX);
    return TRUE;
}

boolean async_read_http(int fd, buffer_t b, HTTP_READ_CALLBACK cb) {
    if (!vec_check_index(&HTTP_CTX, fd)) {
        vec_resize(&HTTP_CTX, fd);
    }

    http_ctx_t ctx = vec_at(&HTTP_CTX, fd);
    ctx->buffer = b;
    ctx->len_parsed = 0;
    ctx->cb = cb;

    http_parser_settings_init(&(ctx->settings));
    ctx->settings.on_message_begin = on_message_begin;
    ctx->settings.on_message_complete = on_message_complete;
    ctx->settings.on_headers_complete = on_headers_complete;

    http_parser_init(&(ctx->parser), HTTP_BOTH);
    ctx->parser.data = ctx;
    return _read_http_req(fd);
}

boolean _read_http_req(int fd) {
    http_ctx_t ctx = vec_at(&HTTP_CTX, fd);
    buffer_t b = ctx->buffer;

    size_t expect_size = CNT_PREFER + buffer_size(b);
    if (expect_size > buffer_capacity(b)) {
        buffer_resize(b, expect_size);
    }

    return async_read_some(fd, CNT_PREFER, buffer_end(b), _read_cb, FALSE);
}

void _read_cb(int fd, int err, char *dst, size_t size_transferred) {
    http_ctx_t ctx = vec_at(&HTTP_CTX, fd);

    buffer_t buffer = ctx->buffer;

    if (err) {
        ctx->cb(fd, err, buffer, ctx->len_parsed);
        return;
    }

    buffer_set_size(buffer, buffer_size(buffer) + size_transferred);

    size_t len_parsed = http_parser_execute(
            &(ctx->parser),&(ctx->settings), dst,size_transferred);

    if (HTTP_PARSER_ERRNO(&ctx->parser) != HPE_OK) {
        ctx->cb(fd, HTTP_PARSE_ERROR, buffer, ctx->len_parsed);
        return;
    }

    ctx->len_parsed += len_parsed;

    if (ctx->complete) {
        ctx->cb(fd, AIO_ERR_NONE, buffer, ctx->len_parsed);
    } else {
        _read_http_req(fd);
    }
}
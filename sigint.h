//
// Created by json on 22-11-29.
//

#ifndef AIO_SIGINT_H
#define AIO_SIGINT_H

void sigint_init();
void sigint_free();
boolean sigint_check();


#endif //AIO_SIGINT_H

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "log.h"
#include "aio.h"
#include "async_read_http.h"

#define REAL_IP     "192.168.1.228"
#define REAL_PORT   4080

typedef struct st_proxy_channel {
    int front_fd;
    int back_fd;

    int ref;

    buffer_t front2back;
    buffer_t back2front;

} *proxy_channel_t;

proxy_channel_t CTX[1024];

proxy_channel_t proxy_channel_ctor() {
    proxy_channel_t channel = malloc(sizeof (struct st_proxy_channel));
    channel->ref = 0;
    channel->front_fd = 0;
    channel->back_fd = 0;
    channel->front2back = buffer_malloc();
    channel->back2front = buffer_malloc();

    LOG(DEBUG, "CHANNEL CREATE, %p", channel);

    return channel;
}

void proxy_channel_close(proxy_channel_t channel) {
    if (channel->ref == 0) {

        LOG(DEBUG, "CHANNEL DESTROY, %p, front:%d, back:%d", channel, channel->front_fd, channel->back_fd);

        CTX[channel->front_fd] = NULL;
        CTX[channel->back_fd] = NULL;
        close(channel->front_fd);
        close(channel->back_fd);
        buffer_free(channel->front2back);
        buffer_free(channel->back2front);
        free(channel);

    }
}

void proxy_channel_ref_inc(proxy_channel_t channel) {
    channel->ref++;
    //LOG(DEBUG, "CHANNEL %p, ref: %d", channel, channel->ref);
}

void proxy_channel_ref_dec(proxy_channel_t channel) {
    channel->ref--;
    //LOG(DEBUG, "CHANNEL %p, ref: %d", channel, channel->ref);
}


void front_http_read_cb(int fd, int err, buffer_t b, size_t size_transferred);
void back_http_read_cb(int fd, int err, buffer_t b, size_t size_transferred);

void front_write_cb(int fd, int err, const char *src, size_t size_transferred);
void back_write_cb(int fd, int err, const char *src, size_t size_transferred);
void accept_cb(int fd, int err, int accepted_fd);
void connect_cb(int fd, int err);

int main() {
    for (size_t i = 0; i < 1024; ++i) {
        CTX[i] == NULL;
    }

    aio_init();
    http_module_init();
    int listen_fd = tcp_socket();
    tcp_socket_enable_nonblock(listen_fd);
    socket_bind_listen(listen_fd, 8899);
    async_accept(listen_fd, accept_cb, FALSE);

    aio_sync();
    aio_free();
    http_module_free();
    for (size_t i = 0; i < 1024; ++i) {
        if (CTX[i] != NULL) {
            CTX[i]->ref = 0;
            proxy_channel_close(CTX[i]);
        }
    }

    return 0;
}

void accept_cb(int _fd, int err, int accepted_fd) {
    if (err) {
        LOG(INFO, "fd: %d accept err, errno: %d, desc: %s", _fd, err, aio_err_desc(err));
    } else {
        async_accept(_fd, accept_cb, FALSE);

        proxy_channel_t channel = proxy_channel_ctor();
        channel->front_fd = accepted_fd;
        channel->back_fd = tcp_socket();
        tcp_socket_enable_nonblock(channel->back_fd);

        CTX[channel->front_fd] = channel;
        CTX[channel->back_fd] = channel;

        async_connect(channel->back_fd, REAL_IP, REAL_PORT, connect_cb, FALSE);
        proxy_channel_ref_inc(channel);
    }
}

void front_http_read_cb(int fd, int err, buffer_t b, size_t size_transferred) {
    proxy_channel_t channel = CTX[fd];
    proxy_channel_ref_dec(channel);
    if (err) {
        LOG(FATAL, "CHANNEL %p, FRONT READ ERR: %s", channel, aio_err_desc(err));
        proxy_channel_close(channel);
    } else {
        //LOG(INFO,  "CHANNEL %p, FRONT READ OK, %zu BYTE", channel, size_transferred);
        async_write(channel->back_fd, size_transferred, buffer_begin(b), back_write_cb, FALSE);
        proxy_channel_ref_inc(channel);
    }
}
void back_http_read_cb(int fd, int err, buffer_t b, size_t size_transferred) {
    proxy_channel_t channel = CTX[fd];
    proxy_channel_ref_dec(channel);
    if (err) {
        LOG(FATAL, "CHANNEL %p, BACK READ ERR: %s", channel, aio_err_desc(err));
        proxy_channel_close(channel);
    } else {
        //LOG(INFO,  "CHANNEL %p, BACK READ OK, %zu BYTE", channel, size_transferred);
        async_write(channel->front_fd, size_transferred, buffer_begin(b), front_write_cb, FALSE);
        proxy_channel_ref_inc(channel);
    }
}

void front_write_cb(int fd, int err, const char *src, size_t size_transferred) {
    proxy_channel_t channel = CTX[fd];
    proxy_channel_ref_dec(channel);
    if (err) {
        LOG(FATAL, "CHANNEL %p, WRITE FRONT ERR: %s", channel, aio_err_desc(err));
        proxy_channel_close(channel);
    } else {
        //LOG(INFO, "CHANNEL %p, WRITE FRONT OK, %d BYTE", channel, size_transferred);
        buffer_set_size(channel->front2back, 0);
        buffer_set_size(channel->back2front, 0);
        async_read_http(channel->front_fd, channel->front2back, front_http_read_cb);
        proxy_channel_ref_inc(channel);
    }
}
void back_write_cb(int fd, int err, const char *src, size_t size_transferred) {
    proxy_channel_t channel = CTX[fd];
    proxy_channel_ref_dec(channel);
    if (err) {
        LOG(FATAL, "CHANNEL %p, WRITE BACK ERR: %s", channel, aio_err_desc(err));
        proxy_channel_close(channel);
    } else {
        //LOG(INFO, "CHANNEL %p, WRITE BACK OK, %d BYTE", channel, size_transferred);
        async_read_http(channel->back_fd, channel->back2front, back_http_read_cb);
        proxy_channel_ref_inc(channel);
    }
}

void connect_cb(int fd, int err) {
    proxy_channel_t channel = CTX[fd];
    proxy_channel_ref_dec(channel);
    if (err) {
        LOG(FATAL, "PROXY CHANNEL %p ESTABLISH FAIL %s, [FRONT %d --> PROXY --> BACK %d]",
            channel, aio_err_desc(err), channel->front_fd, channel->back_fd);
        proxy_channel_close(channel);
    } else {
        //LOG(INFO, "PROXY CHANNEL %p ESTABLISHED, [FRONT %d --> PROXY --> BACK %d]", channel, channel->front_fd, channel->back_fd);
        //async_read_some(channel->front_fd, CNT_PREFER, malloc(CNT_PREFER), front_read_cb, FALSE);
        async_read_http(channel->front_fd, channel->front2back, front_http_read_cb);
        proxy_channel_ref_inc(channel);

    }
}

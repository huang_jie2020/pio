//
// Created by json on 22-12-19.
//

#ifndef AIO_ASYNC_NET_H
#define AIO_ASYNC_NET_H

#include <stddef.h>
#include <inttypes.h>
#include <netinet/in.h>
#include <stdbool.h>

typedef void* context_t;

typedef enum {
    IO_ERR_NONE = 0,    /* SUCCESS */
    IO_ERR_FIN,         /* READ FIN */
    IO_ERR_TIMEOUT,     /* IO TIMEOUT */
    IO_ERR_USER_ABORT,  /* IO ABORT BY USER */
    IO_ERR_SYS_CALL,    /* consult errno for details */
} io_err_type_t;

typedef struct st_io_err {
    io_err_type_t   io_err_type;
    int             errno;
} *io_err_t;

typedef struct st_event_proactor *event_proactor_t;
typedef struct st_acceptor *acceptor_t;
typedef struct st_socket   *socket_t;

typedef void (*READ_CALLBACK)   (socket_t s,    io_err_t err, void       *buf, size_t size_transferred, context_t ctx);
typedef void (*WRITE_CALLBACK)  (socket_t s,    io_err_t err, const void *buf, size_t size_transferred, context_t ctx);
typedef void (*CONNECT_CALLBACK)(socket_t s,    io_err_t err,                                           context_t ctx);
typedef void (*ACCEPT_CALLBACK) (acceptor_t a,  io_err_t err, socket_t s, context_t ctx);
typedef void (*TASK_RUN)        (context_t ctx);

typedef bool (*ASYNC_READ_FUNC)     (socket_t s, size_t size_to_read, void *buf, READ_CALLBACK cb, size_t expire, bool some_flag, context_t ctx);
typedef bool (*ASYNC_WRITE_FUNC)    (socket_t s, size_t size_to_write, void *buf, WRITE_CALLBACK cb, size_t expire, context_t ctx);
typedef bool (*ASYNC_CONNECT_FUNC)  (socket_t s, const char *ip, unsigned short port, CONNECT_CALLBACK, size_t expire, context_t ctx);
typedef bool (*ASYNC_ACCEPT_FUNC)   (acceptor_t a, socket_t s, ACCEPT_CALLBACK cb, context_t ctx);
typedef bool (*CANCEL_READ)

struct st_socket {
    const ASYNC_CONNECT_FUNC    async_connect;
    const ASYNC_READ_FUNC       async_read;
    const ASYNC_WRITE_FUNC      async_write;
};

struct st_acceptor {
    const ASYNC_ACCEPT_FUNC     async_accept;
};

event_proactor_t event_proactor_epoll();

acceptor_t  acceptor_for_listen(event_proactor_t event_proactor, unsigned int port);
socket_t    socket_for_connect(event_proactor_t event_proactor);
socket_t    socket_for_accept(event_proactor_t event_proactor);

bool async_write    (socket_t s, size_t size_to_write, void *buf, WRITE_CALLBACK cb, size_t expire, context_t ctx);
bool async_connect  (socket_t s, const char *ip, unsigned short port, CONNECT_CALLBACK, size_t expire, context_t ctx);

bool async_accept   (acceptor_t a, socket_t s, ACCEPT_CALLBACK cb, context_t ctx);

bool cancel_read
bool cancel_write(socket_t s);

#endif //AIO_ASYNC_NET_H

//
// Created by json on 2022/12/12.
//

#ifndef AIO_TIMING_WHEEL_H
#define AIO_TIMING_WHEEL_H

#include "type_def.h"


boolean         timing_wheel_module_init();
void            timing_wheel_module_free();

//
delay_task_t    delay_task_ctor(TASK_ROUTINE run, void *arg);
boolean         delay_task_is_scheduled(delay_task_t task);
void            delay_task_release(delay_task_t task);
//
boolean         timing_wheel_add(size_t expire /* ms */,delay_task_t task);
void            timing_wheel_rm(delay_task_t task);

#endif //AIO_TIMING_WHEEL_H

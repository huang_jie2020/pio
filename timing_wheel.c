//
// Created by json on 2022/12/12.
//

#include <stdlib.h>
#include <sys/timerfd.h>
#include <unistd.h>
#include <assert.h>

#include "log.h"
#include "timing_wheel.h"
#include "aio.h"

#define TW_LVL_MILLI_SEC_CNT    10
#define TW_LVL_SEC_CNT          60
#define TW_LVL_MIN_CNT          60
#define TW_LVL_HOUR_CNT         24


#define TICK_INTERVAL   (1000000000/TW_LVL_MILLI_SEC_CNT)

#define TTT (TW_LVL_MILLI_SEC_CNT * TW_LVL_SEC_CNT * TW_LVL_MIN_CNT * TW_LVL_HOUR_CNT)


typedef struct st_timing_wheel  *timing_wheel_t;

struct st_delay_task {
    struct {
        TASK_ROUTINE run;
        void *arg;
    } cb;

    long add_at;   /* ms */
    size_t expire;   /* ms */

    timing_wheel_t tw;  // now in which tw

    delay_task_t prev;
    delay_task_t next;
};

struct st_timing_wheel {
    delay_task_t slots;
    uint16_t c;         // the count of slots;
    uint16_t pos;       // current pos;

    size_t base;

    const char *name;
    timing_wheel_t up;
    timing_wheel_t down;
};

static int          TIMER_FD = -1;
static uint64_t     EXP = 0;

static timing_wheel_t   TW_HEAD;      // milli-sec timing-wheel
static timing_wheel_t   TW_TAIL;      // hour timing-wheel
static int             CURRENT_TICK;

static void tick(int fd, int err, char *dst, size_t size_transferred);

timing_wheel_t timing_wheel_ctor(uint16_t c, const char *name, size_t base) {
    timing_wheel_t tw = malloc(sizeof (*tw));
    tw->c = c;
    tw->slots = calloc(tw->c, sizeof (struct st_delay_task));
    for (uint16_t i = 0; i < c; ++i) {
        // init the list head;
        tw->slots[i].cb.run = NULL;
        tw->slots[i].cb.arg = NULL;
        tw->slots[i].expire = 0;
        tw->slots[i].tw     = tw;
        tw->slots[i].prev = NULL;
        tw->slots[i].next = NULL;
    }
    tw->pos = 0;
    tw->name = name;
    tw->base = base;
    tw->up = NULL;
    tw->down = NULL;
    return tw;
}


static struct timeval START;
long TT = 0;

boolean timing_wheel_module_init() {
    timing_wheel_t tw_millisecond   = timing_wheel_ctor(TW_LVL_MILLI_SEC_CNT, "TW_MILLI_SECOND", 1);
    timing_wheel_t tw_second        = timing_wheel_ctor(TW_LVL_SEC_CNT, "TW_SECOND", TW_LVL_MILLI_SEC_CNT);
    timing_wheel_t tw_minute        = timing_wheel_ctor(TW_LVL_MIN_CNT, "TW_MINUTE", TW_LVL_MILLI_SEC_CNT * TW_LVL_SEC_CNT);
    timing_wheel_t tw_hour          = timing_wheel_ctor(TW_LVL_HOUR_CNT, "TW_HOUR", TW_LVL_MILLI_SEC_CNT * TW_LVL_SEC_CNT * TW_LVL_MIN_CNT);

    tw_millisecond->up = tw_second;
    tw_millisecond->down = NULL;

    tw_second->up = tw_minute;
    tw_second->down = tw_millisecond;

    tw_minute->up = tw_hour;
    tw_minute->down = tw_second;

    tw_hour->up = NULL;
    tw_hour->down = tw_minute;

    TW_HEAD = tw_millisecond;
    TW_TAIL = tw_hour;

    CURRENT_TICK = 0;


    TIMER_FD = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK | TFD_CLOEXEC);
    struct itimerspec value;
    value.it_interval.tv_sec = 0;
    value.it_interval.tv_nsec = 0;
    value.it_value.tv_sec = 0;
    value.it_value.tv_nsec = TICK_INTERVAL;


    gettimeofday(&START, NULL);
    TT = TICK_INTERVAL;

    timerfd_settime(TIMER_FD, 0, &value, NULL);
    async_read(TIMER_FD, sizeof EXP, (char *)&EXP, tick, 0);

    return TRUE;
}

void timing_wheel_module_free() {
    timing_wheel_t tw = TW_HEAD;
    while (tw != NULL) {
        timing_wheel_t next = tw->up;
        free(tw);
        tw = next;
    }
    close(TIMER_FD);
}

void tick(int fd, int err, char *dst, size_t size_transferred) {
    timing_wheel_t tw = TW_HEAD;
    while (tw != NULL) {
        if (1 + tw->pos < tw->c) {
            tw->pos++;
            tw = NULL;
        } else {
            tw->pos = 0;
            tw = tw->up;
        }
    }
    CURRENT_TICK++;
    if (CURRENT_TICK >= TTT) {
        CURRENT_TICK = 0;
    }
    //LOG(DEBUG, "AT %d, TICK", CURRENT_TICK);


    // adjust
    tw = TW_TAIL;
    while (tw != TW_HEAD) {
        delay_task_t list_head = tw->slots + tw->pos;
        delay_task_t it = list_head->next;
        while (it != NULL) {
            delay_task_t next = it->next;
            size_t offset = CURRENT_TICK - it->add_at > 0 ?
                    CURRENT_TICK - it->add_at : CURRENT_TICK - it->add_at + TTT;
            it->tw = NULL;
            timing_wheel_add(it->expire - offset, it);
            it = next;
        }
        list_head->next = NULL;
        tw = tw->down;
    }

    // callback
    tw = TW_HEAD;
    delay_task_t list_head = tw->slots + tw->pos;
    delay_task_t it = list_head->next;
    while (it != NULL) {
        delay_task_t next = it->next;
        LOG(INFO, "AT %d, TRIGGER TASK %p", CURRENT_TICK, it);
        TASK_ROUTINE it_run = it->cb.run;
        void        *it_arg = it->cb.arg;
        // it can be released in task run, so we update it at first
        it->expire = 0;
        it->prev = NULL;
        it->next = NULL;
        it->tw = NULL;

        it_run(it_arg);
        it = next;
    }
    list_head->next = NULL;

    struct timeval tv;
    gettimeofday(&tv, NULL);



    double t1 = 1000000000.0 * START.tv_sec + 1000.0 * START.tv_usec;
    double t2 = 1000000000.0 * tv.tv_sec + 1000.0 * tv.tv_usec;
    long offset = t2 - t1 - TT;

    offset = offset < TICK_INTERVAL ? offset : 0;

    //LOG(DEBUG, "%f", (double)offset/TT);
    if (((double)offset/TT) >= 1.0f ) {
        assert(FALSE);
    }

    //LOG(DEBUG, "offset %d", offset);

    struct itimerspec value;
    value.it_interval.tv_sec = 0;
    value.it_interval.tv_nsec = 0;
    value.it_value.tv_sec = 0;
    value.it_value.tv_nsec = TICK_INTERVAL - offset;

    START = tv;
    TT = value.it_value.tv_nsec;
    timerfd_settime(TIMER_FD, 0, &value, NULL);
    async_read(TIMER_FD, sizeof EXP, (char *)&EXP, tick, 0);
}

boolean timing_wheel_add(size_t expire /* ms */, delay_task_t task) {
    assert(task->tw == NULL);

    LOG(INFO, "AT %d, ADD TASK BEGIN %p", CURRENT_TICK, task);

    task->add_at = CURRENT_TICK;
    task->expire = expire;

    task->expire = expire;
    timing_wheel_t tw = TW_HEAD;
    while (tw != NULL) {
        size_t k = (expire + CURRENT_TICK % tw->base) / tw->base;
        uint16_t pos = 0;
        if (k >= tw->c && tw->up != NULL) {
            tw = tw->up;
        } else {
            if (k < tw->c) {
                pos = k + tw->pos;
            } else {
                pos = tw->c - 1 + tw->pos;
            }
            pos = pos >= tw->c ? pos - tw->c : pos;
            delay_task_t list_head = tw->slots + pos;

            delay_task_t next = list_head->next;
            task->next = next;
            task->prev = list_head;
            if (next != NULL) {
                next->prev = task;
            }
            list_head->next = task;

            task->tw = tw;
            break;
        }
    }
    LOG(INFO, "AT %d, ADD TASK END %p", CURRENT_TICK, task);
    return TRUE;
}

void timing_wheel_rm(delay_task_t task) {
    assert(task->tw != NULL);

    timing_wheel_t tw = task->tw;
    uint16_t pos = task->expire / tw->base;
    delay_task_t list_head = tw->slots + pos;

    delay_task_t prev = task->prev;
    delay_task_t next = task->next;

    assert(prev != NULL);

    task->expire = 0;
    task->prev = NULL;
    task->next = NULL;
    task->tw = NULL;

    prev->next = next;
    if (next != NULL) {
        next->prev = prev;
    }
}

delay_task_t delay_task_ctor(TASK_ROUTINE run, void *arg) {
    delay_task_t task = malloc(sizeof (*task));
        task->cb.run = run;
        task->cb.arg = arg;
        task->expire = 0;
        task->tw = NULL;
        task->prev = NULL;
        task->next = NULL;
    return task;
}

boolean delay_task_is_scheduled(delay_task_t task) {
    return task->tw != NULL;
}

void delay_task_release(delay_task_t task) {
    free(task);
}
//
// Created by json on 2022/11/15.
//

#ifndef AIO_EPOLL_HELPER_H
#define AIO_EPOLL_HELPER_H

#include "type_def.h"
#include <sys/epoll.h>

boolean epoll_init();
boolean epoll_free();
boolean epoll_add(int fd, int evt);
boolean epoll_remove(int fd, int evt);
boolean epoll_poll(struct epoll_event *events, int maxevents, int timeout, int *fs);

#endif //AIO_EPOLL_HELPER_H

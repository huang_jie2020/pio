//
// Created by json on 22-11-27.
//

#ifndef AIO_BUFFER_H
#define AIO_BUFFER_H

#include "type_def.h"

typedef struct st_buffer *buffer_t;

buffer_t buffer_malloc();
buffer_t buffer_free(buffer_t b);
size_t  buffer_capacity(buffer_t b);
size_t  buffer_size(buffer_t b);
void    buffer_set_size(buffer_t b, size_t new_size);
boolean buffer_resize(buffer_t b, size_t i);
char *  buffer_begin(buffer_t b);
char *  buffer_end(buffer_t b);


#endif //AIO_BUFFER_H

//
// Created by json on 22-12-19.
//


#include "async_net.h"

typedef struct st_io_complete *io_complete_t;
typedef struct st_fd_base *fd_base_t;


typedef bool (*BEGIN_READ)  (fd_base_t this);
typedef bool (*SYNC_READ)   (fd_base_t this);
typedef bool (*END_READ)    (fd_base_t this, io_complete_t it);
typedef bool (*BEGIN_WRITE) (fd_base_t this);
typedef bool (*SYNC_WRITE)  (fd_base_t this);
typedef bool (*END_WRITE)   (fd_base_t this, io_complete_t it);

bool default_not_implemented1(fd_base_t this);
bool default_not_implemented2(fd_base_t this, io_complete_t it);


struct st_fd_base {
    int             fd;
    bool            listening_readable;
    bool            listening_writable;
    const char      *desc_debug;

    BEGIN_READ      begin_read;
    SYNC_READ       sync_read;
    END_READ        end_read;
    BEGIN_WRITE     begin_write;
    SYNC_WRITE      sync_write;
    END_WRITE       end_write;
};

struct st_acceptor {
    struct st_fd_base base;

    context_t           ctx;
    ACCEPT_CALLBACK     cb;
    socket_t            s;
};



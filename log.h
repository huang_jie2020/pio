//
// Created by json on 2022/11/15.
//

#ifndef AIO_LOG_H
#define AIO_LOG_H

#include <stdio.h>
#include <sys/prctl.h>
#include <sys/time.h>
#include <time.h>


#define DEBUG       "DEBUG"
#define TRACE       "TRACE"
#define INFO        "INFO "
#define WARN        "WARN "
#define FATAL       "FATAL"


#define LOG(level, format, ...)             \
do {                                        \
    char t[28] = { 0 };                     \
    struct timeval tTimeVal;                \
    gettimeofday(&tTimeVal, NULL);          \
    struct tm *tTM = localtime(&tTimeVal.tv_sec);\
    sprintf(t, "%04d-%02d-%02d %02d:%02d:%02d.%01ld",\
        tTM->tm_year + 1900, tTM->tm_mon + 1, tTM->tm_mday,\
        tTM->tm_hour, tTM->tm_min, tTM->tm_sec,\
        tTimeVal.tv_usec / 100000);\
    char name[20] = { 0 };                  \
    prctl(PR_GET_NAME, name);               \
    fprintf(stdout, "[%s][%s][%s][%s][ %s:%d]" format "\n", \
    level, t, name, __func__, __FILE__, __LINE__, ##__VA_ARGS__ ); \
} while (0)


#endif //AIO_LOG_H

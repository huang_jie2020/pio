//
// Created by json on 22-12-17.
//

#ifndef AIO_VECTOR_TEMPLATE_H
#define AIO_VECTOR_TEMPLATE_H

#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#define VECTOR_TEMPLATE_INSTANTIATION(E, T, PTR) \
typedef struct T {                               \
    size_t c;                                    \
    size_t s;                                    \
    E   *d;                                      \
} *PTR                                           \

#define VECTOR_INIT(vec, capacity)                      \
do {                                                    \
    (vec)->c = (capacity);                              \
    (vec)->d = calloc((vec)->c, sizeof(*((vec)->d)));   \
} while (false)                                         \

#define VECTOR_FREE(vec)            \
do {                                \
    free((vec)->d);                 \
    (vec)->d = NULL;                \
    (vec)->s = 0;                   \
    (vec)->c = 0;                   \
} while (false)                     \

#define VECTOR_RESIZE(vec, i)                                                           \
do {                                                                                    \
    size_t old_c = (vec)->c;                                                            \
    size_t new_c = old_c + old_c / 2 < INT_MAX ? old_c + old_c / 2 : INT_MAX;           \
    new_c = new_c > i ? new_c : 1 + (i);                                                \
    (vec)->c = new_c;                                                                   \
    (vec)->d = realloc((vec)->d, new_c * sizeof(*((vec)->d)));                          \
    memset((vec)->d + old_c, 0, (new_c - old_c) * sizeof(*((vec)->d)));                 \
} while (false)                                                                         \

#define VECTOR_AT(vec, i) ((vec)->d + (i))

#define VECTOR_PUSH_BACK(vec, item)         \
do {                                        \
    if ((vec)->s >= (vec)->c)               \
        VECTOR_RESIZE((vec), (vec)->s);     \
    (vec)->d[(vec)->s++] = (item);          \
} while (false)                             \

#endif //AIO_VECTOR_TEMPLATE_H

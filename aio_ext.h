//
// Created by json on 22-12-10.
//

#ifndef AIO_AIO_EXT_H
#define AIO_AIO_EXT_H

#include "type_def.h"

void aio_ext_init();

// can be called in any thread
// run will be called in AIO THREAD
boolean aio_run_task(TASK_ROUTINE run, void *arg);
// run will be called in WORKER THREAD
boolean aio_post_task(TASK_ROUTINE run, void *arg);

#endif //AIO_AIO_EXT_H

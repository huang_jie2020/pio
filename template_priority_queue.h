//
// Created by json on 22-12-22.
//

#ifndef AIO_TEMPLATE_PRIORITY_QUEUE_H
#define AIO_TEMPLATE_PRIORITY_QUEUE_H

#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>

#define PRIORITY_QUEUE_TEMPLATE_INSTANTIATION(E, T, PTR) \
typedef struct T {                                       \
    size_t  c;                                           \
    size_t  s;                                           \
    struct {int p; E d;} t;                              \
    struct {                                             \
        int p;                                           \
        E   d;                                           \
    } *v;                                                \
} *PTR                                                   \

#define PRIORITY_QUEUE_INIT(pq, capacity)           \
do {                                                \
    (pq)->c = (capacity);                           \
    (pq)->s = 0;                                    \
    (pq)->v = calloc((pq)->c, sizeof(*(pq)->v));    \
} while (false)                                     \

#define PRIORITY_QUEUE_FREE(pq) \
do {                            \
    free((pq)->v);              \
    pq->v = NULL;               \
    pq->c = 0;                  \
    pq->s = 0;                  \
} while (false)                 \

#define PRIORITY_QUEUE_CAPACITY(pq) ((pq)->c)

#define PRIORITY_QUEUE_SIZE(pq) ((pq)->s)

#define PRIORITY_QUEUE_SWAP(pq, m, n) \
do {                                  \
    pq->t.d = pq->v[m].d;             \
    pq->t.p = pq->v[m].p;             \
    pq->v[m].d = pq->v[n].d;          \
    pq->v[m].p = pq->v[n].p;          \
    pq->v[n].d = pq->t.d;             \
    pq->v[n].p = pq->t.p;             \
} while (false)                       \

#define PRIORITY_QUEUE_IS_LEAF_NODE(pq, m) (2*(m)+1 >= (pq)->s)

#define PRIORITY_QUEUE_MAX_SON(pq, par)                 \
2*(par)+2 >= (pq)->s ? 2*(par)+1 :                      \
    ((pq)->v[2*(par)+1].p > (pq)->v[2*(par)+2].p ?      \
        2*(par)+1 : 2*(par)+2)                          \


#define PRIORITY_QUEUE_PUSH(pq, t, pri)                       \
do {                                                        \
    (pq)->v[(pq)->s].d = *(t);                              \
    (pq)->v[(pq)->s].p = (pri);                               \
    size_t son = (pq)->s;                                   \
    size_t par = (son - 1) / 2;                             \
    while (son != 0 && (pq)->v[son].p > (pq)->v[par].p) {     \
        PRIORITY_QUEUE_SWAP((pq), son, par);                \
        son = par;                                          \
        par = (son - 1) / 2;                                \
    }                                                       \
    (pq)->s++;                                              \
} while (false)                                             \

#define PRIORITY_QUEUE_POP(pq, t, pri)                      \
do {                                                        \
    *(t) = (pq)->v[0].d;                                    \
    *(pri) = (pq)->v[0].p;                                  \
    PRIORITY_QUEUE_SWAP((pq), 0, (pq)->s-1);                \
    (pq)->s--;                                              \
    size_t par = 0;                                         \
    size_t son = PRIORITY_QUEUE_MAX_SON((pq), par);         \
    while (!PRIORITY_QUEUE_IS_LEAF_NODE((pq), par) &&       \
            (pq)->v[par].p < (pq)->v[son].p) {              \
        PRIORITY_QUEUE_SWAP((pq), son, par);                \
        par = son;                                          \
        son = PRIORITY_QUEUE_MAX_SON((pq), par);            \
    }                                                       \
} while (false)                                             \






#endif //AIO_TEMPLATE_PRIORITY_QUEUE_H

//
// Created by json on 2022/11/15.
//
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include "vector.h"

#include "epoll_helper.h"
#include "log.h"

#define EPOLLNONE 0

typedef struct st_fd_mask {
    int mask;
} *fd_mask_t ;

static __thread int EPFD;

static __thread struct {
#define MAX_CAPACITY        1024000
#define INITIAL_CAPACITY    1024

    fd_mask_t   d;
    size_t      c;
} FD_EVT_STATE;

boolean epoll_init() {
    EPFD = epoll_create(INITIAL_CAPACITY);
    if (EPFD == -1) {
        LOG(FATAL, "call epoll_create error, errno: %d, desc: %s", errno, strerror(errno));
        return FALSE;
    }
    vec_init(&FD_EVT_STATE);
    return TRUE;
}

boolean epoll_free() {
    int r = close(EPFD);
    if (r == -1) {
        LOG(FATAL, "call close error");
        return FALSE;
    }
    vec_free(&FD_EVT_STATE);
    return TRUE;
}

boolean epoll_add(int fd, int evt) {
    if (fd < 0) {
        LOG(FATAL, "call epoll_add with wrong fd: %d, evt: %d", fd, evt);
        return FALSE;
    }

    if (!vec_check_index(&FD_EVT_STATE, fd)) {
        vec_resize(&FD_EVT_STATE, fd);
    }

    fd_mask_t me = vec_at(&FD_EVT_STATE, fd);

    int mask = me->mask;
    if (mask & evt)
        return TRUE;
    int op = mask == EPOLLNONE ? EPOLL_CTL_ADD : EPOLL_CTL_MOD;
    mask |= evt;

    struct epoll_event ee;
    memset(&ee, 0, sizeof(ee));
    ee.data.fd = fd;
    ee.events = mask;

    int t = epoll_ctl(EPFD, op, fd, &ee);
    if (-1 == t) {
        LOG(FATAL, "call epoll_ctl error, errno: %d, desc: %s", errno, strerror(errno));
        return FALSE;
    }
    me->mask = mask;
    return TRUE;

}

boolean epoll_remove(int fd, int evt) {
    if (!vec_check_index(&FD_EVT_STATE, fd)) {
        LOG(FATAL, "call epoll_remove with wrong fd: %d, evt: %d", fd, evt);
        return FALSE;
    }

    fd_mask_t me = vec_at(&FD_EVT_STATE, fd);

    int mask = me->mask;
    if (!(mask & evt))
        return TRUE;

    mask &= (~evt);
    int op = mask == EPOLLNONE ? EPOLL_CTL_DEL : EPOLL_CTL_MOD;

    struct epoll_event ee;
    memset(&ee, 0, sizeof(ee));
    ee.data.fd = fd;
    ee.events = mask;

    int r = epoll_ctl(EPFD, op, fd, &ee);
    if (-1 == r) {
        LOG(FATAL, "call epoll_ctl error, errno: %d, desc: %s", errno, strerror(errno));
        return FALSE;
    }
    me->mask = mask;
    return TRUE;
}

boolean epoll_poll(struct epoll_event *events, int maxevents, int timeout, int *fs) {
    int r = epoll_wait(EPFD, events, maxevents, timeout);
    if (-1 == r) {
        if (errno != EINTR) {
            LOG(FATAL, "call epoll_wait error, errno: %d, desc: %s", errno, strerror(errno));
            return FALSE;
        }
        *fs = 0;
    } else {
        *fs = r;
    }
    return TRUE;
}
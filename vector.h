//
// Created by json on 22-11-26.
//

#ifndef AIO_VECTOR_H
#define AIO_VECTOR_H

#include <stdlib.h>
#include <string.h>

#define vec_init(vec) \
do {                  \
    (vec)->c = INITIAL_CAPACITY;             \
    (vec)->d = calloc((vec)->c, sizeof (*((vec)->d)));   \
    memset((vec)->d, 0, (vec)->c * sizeof (*((vec)->d)));\
} while (FALSE)                            \

#define vec_free(vec) \
do {                  \
    if (NULL != (vec)->d) \
        free((vec)->d); \
    (vec)->c = 0;       \
    (vec)->d = NULL;\
} while (FALSE)\

#define vec_at(vec, i) ((vec)->d + (i))

#define vec_capacity(vec) ((vec)->c)

#define vec_resize(vec, i) \
do {                       \
    size_t old_c = (vec)->c; \
    size_t new_c = old_c + old_c / 2 < MAX_CAPACITY ? old_c + old_c / 2 : MAX_CAPACITY; \
    new_c = new_c > i ? new_c : 1 + (i);                  \
    (vec)->c = new_c;        \
    (vec)->d = realloc((vec)->d, new_c * sizeof(*((vec)->d))); \
    memset((vec)->d + old_c, 0, (new_c - old_c) * sizeof(*((vec)->d)));\
} while (FALSE)            \

#define vec_check_index(vec, i) ((i) >= 0 && (i) < (vec)->c)

#endif //AIO_VECTOR_H

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#include "log.h"
#include "aio.h"
#include "aio_ext.h"
#include "timing_wheel.h"


void read_cb(int fd, int err, char *dst, size_t size_transferred);
void write_cb(int fd, int err, const char *src, size_t size_transferred);
void accept_cb(int fd, int err, int accepted_fd);


void CB(void *d) {
    LOG(INFO, "CB %d", (int)d);
}
void RUN(void *d) {
    LOG(INFO, "BEGIN HARD TASK %d", (int)d);
    sleep((int)d);
    LOG(INFO, "END   HARD TASK %d", (int)d);
    aio_run_task(CB, d);
}

void RUN1(void *d) {
    LOG(INFO, "RUN1 %d", (int)d);
}



int main() {
    aio_init();

    offsetof()

    int listen_fd = tcp_socket();
    tcp_socket_enable_nonblock(listen_fd);
    socket_bind_listen(listen_fd, 9091);
    async_accept(listen_fd, accept_cb, 0);

    aio_post_task(RUN, 1);
    aio_post_task(RUN, 1);
    aio_post_task(RUN, 2);
    aio_post_task(RUN, 2);

    aio_sync();

    return 0;
}


void accept_cb(int _fd, int err, int accepted_fd) {
    if (err) {
        LOG(INFO, "fd: %d accept err, errno: %d, desc: %s", _fd, err, aio_err_desc(err));
    } else {

        //timing_wheel_add(100, delay_task_ctor(RUN1, (void*)1));
        //timing_wheel_add(30, delay_task_ctor(RUN1, (void*)1));
        //timing_wheel_add(30, delay_task_ctor(RUN1, (void*)1));

        async_accept(_fd, accept_cb, 0);
        async_read(accepted_fd, 1, malloc(1), read_cb, 300);
    }
}
void read_cb(int fd, int err, char *dst, size_t size_transferred) {
    if (err) {
        LOG(INFO, "fd: %d read err, errno: %d, desc: %s", fd, err, aio_err_desc(err));
        if (err == AIO_ERR_TIMEOUT) {
            shutdown(fd, SHUT_WR);
            async_read(fd, 1, dst, read_cb, 0);
        } else {
            close(fd);
        }
    } else {
        LOG(INFO, "fd: %d read %zu char", fd, size_transferred);
        async_write(fd, 1, (const char*)dst, write_cb, 0);
    }
}

void write_cb(int fd, int err, const char *src, size_t size_transferred) {
    if (err) {
        LOG(INFO, "fd: %d write err, errno: %d, desc: %s", fd, err, aio_err_desc(err));
    } else {
        LOG(INFO, "fd: %d write %zu char", fd, size_transferred);
    }
    async_read(fd, 1, (char *)src, read_cb, 0);
}

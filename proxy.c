#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "log.h"
#include "aio.h"
#include "async_read_http.h"

#define REAL_IP     "112.21.190.5"
#define REAL_PORT   443

#define CNT_PREFER  1024

typedef struct st_proxy_channel {
    int front_fd;
    int back_fd;

    int ref;
    char *front2back;
    char *back2front;
} *proxy_channel_t;

proxy_channel_t CTX[1024];

proxy_channel_t proxy_channel_ctor() {
    proxy_channel_t channel = malloc(sizeof *channel);
    channel->ref = 0;
    channel->front_fd = 0;
    channel->back_fd = 0;
    channel->front2back = malloc(CNT_PREFER);
    channel->back2front = malloc(CNT_PREFER);

    LOG(DEBUG, "CHANNEL CREATE, %p", channel);

    return channel;
}

void proxy_channel_close(proxy_channel_t channel) {
    if (channel->ref == 0) {
        CTX[channel->front_fd] = NULL;
        CTX[channel->back_fd] = NULL;
        close(channel->front_fd);
        close(channel->back_fd);
        free(channel->front2back);
        free(channel->back2front);
        free(channel);

        LOG(DEBUG, "CHANNEL DESTROY, %p", channel);
    }
}

void proxy_channel_ref_inc(proxy_channel_t channel) {
    channel->ref++;
    //LOG(DEBUG, "CHANNEL %p, ref: %d", channel, channel->ref);
}

void proxy_channel_ref_dec(proxy_channel_t channel) {
    channel->ref--;
    //LOG(DEBUG, "CHANNEL %p, ref: %d", channel, channel->ref);
}


void front_read_cb(int fd, int err, char *dst, size_t size_transferred);
void back_read_cb(int fd, int err, char *dst, size_t size_transferred);

void front_write_cb(int fd, int err, const char *src, size_t size_transferred);
void back_write_cb(int fd, int err, const char *src, size_t size_transferred);
void accept_cb(int fd, int err, int accepted_fd);
void connect_cb(int fd, int err);

int main() {
    for (size_t i = 0; i < 1024; ++i) {
        CTX[i] == NULL;
    }

    aio_init();

    int listen_fd = tcp_socket();
    tcp_socket_enable_nonblock(listen_fd);
    socket_bind_listen(listen_fd, 8899);
    async_accept(listen_fd, accept_cb, TRUE);

    aio_sync();

    return 0;
}

void accept_cb(int _fd, int err, int accepted_fd) {
    if (err) {
        LOG(INFO, "fd: %d accept err, errno: %d, desc: %s", _fd, err, aio_err_desc(err));
    } else {
        async_accept(_fd, accept_cb, FALSE);

        proxy_channel_t channel = proxy_channel_ctor();
        channel->front_fd = accepted_fd;
        channel->back_fd = tcp_socket();
        tcp_socket_enable_nonblock(channel->back_fd);

        CTX[channel->front_fd] = channel;
        CTX[channel->back_fd] = channel;

        async_connect(channel->back_fd, REAL_IP, REAL_PORT, connect_cb, FALSE);
        proxy_channel_ref_inc(channel);
    }
}

void front_read_cb(int fd, int err, char *dst, size_t size_transferred) {
    proxy_channel_t channel = CTX[fd];
    proxy_channel_ref_dec(channel);
    if (err) {
        LOG(FATAL, "CHANNEL %p, FRONT READ ERR: %s", channel, aio_err_desc(err));
        proxy_channel_close(channel);
    } else {
        //LOG(INFO,  "CHANNEL %p, FRONT READ OK, %d BYTE", channel, size_transferred);
        async_write(channel->back_fd, size_transferred, (const char*)dst, back_write_cb, FALSE);
        proxy_channel_ref_inc(channel);
    }
}
void back_read_cb(int fd, int err, char *dst, size_t size_transferred) {
    proxy_channel_t channel = CTX[fd];
    proxy_channel_ref_dec(channel);
    if (err) {
        LOG(FATAL, "CHANNEL %p, BACK READ ERR: %s", channel, aio_err_desc(err));
        proxy_channel_close(channel);
    } else {
        //LOG(INFO,  "CHANNEL %p, BACK READ OK, %d BYTE", channel, size_transferred);
        async_write(channel->front_fd, size_transferred, (const char*)dst, front_write_cb, FALSE);
        proxy_channel_ref_inc(channel);
    }
}

void front_write_cb(int fd, int err, const char *src, size_t size_transferred) {
    proxy_channel_t channel = CTX[fd];
    proxy_channel_ref_dec(channel);
    if (err) {
        LOG(FATAL, "CHANNEL %p, WRITE FRONT ERR: %s", channel, aio_err_desc(err));
        proxy_channel_close(channel);
    } else {
        //LOG(INFO, "CHANNEL %p, WRITE FRONT OK, %d BYTE", channel, size_transferred);
        async_read_some(channel->back_fd, CNT_PREFER, (char*)src, back_read_cb, FALSE);
        proxy_channel_ref_inc(channel);
    }
}
void back_write_cb(int fd, int err, const char *src, size_t size_transferred) {
    proxy_channel_t channel = CTX[fd];
    proxy_channel_ref_dec(channel);
    if (err) {
        LOG(FATAL, "CHANNEL %p, WRITE BACK ERR: %s", channel, aio_err_desc(err));
        proxy_channel_close(channel);
    } else {
        //LOG(INFO, "CHANNEL %p, WRITE BACK OK, %d BYTE", channel, size_transferred);
        async_read_some(channel->front_fd, CNT_PREFER, (char*)src, front_read_cb, FALSE);
        proxy_channel_ref_inc(channel);
    }
}

void connect_cb(int fd, int err) {
    proxy_channel_t channel = CTX[fd];
    proxy_channel_ref_dec(channel);
    if (err) {
        LOG(FATAL, "PROXY CHANNEL %p ESTABLISH FAIL, [FRONT %d --> PROXY --> BACK %d]",
            channel, channel->front_fd, channel->back_fd);
        proxy_channel_close(channel);
    } else {
        //LOG(INFO, "PROXY CHANNEL %p ESTABLISHED, [FRONT %d --> PROXY --> BACK %d]", channel, channel->front_fd, channel->back_fd);
        async_read_some(channel->front_fd, CNT_PREFER, malloc(CNT_PREFER), front_read_cb, FALSE);
        proxy_channel_ref_inc(channel);
        async_read_some(channel->back_fd, CNT_PREFER, malloc(CNT_PREFER), back_read_cb, FALSE);
        proxy_channel_ref_inc(channel);
    }
}

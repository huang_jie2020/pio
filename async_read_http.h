//
// Created by json on 22-11-24.
//

#ifndef AIO_ASYNC_READ_HTTP_H
#define AIO_ASYNC_READ_HTTP_H

#include "type_def.h"
#include "buffer.h"

boolean http_module_init();
boolean http_module_free();
boolean async_read_http(int fd, buffer_t b, HTTP_READ_CALLBACK cb);


#endif //AIO_ASYNC_READ_HTTP_H

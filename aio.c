//
// Created by json on 2022/11/22.
//

#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <signal.h>

#include <assert.h>
#include <sys/prctl.h>
#include <pthread.h>

#include "vector.h"
#include "ring_queue.h"
#include "aio.h"
#include "epoll_helper.h"
#include "aio_ext.h"
#include "sigint.h"
#include "log.h"
#include "timing_wheel.h"


#define IO_NONE                 0
#define IO_READ_SPECIFIC_COUNT  10
#define IO_READ_SOME            11
#define IO_READ_UNTIL           12
#define IO_WRITE_SPECIFIC_COUNT 20
#define IO_ACCEPT               30
#define IO_CONNECT              40

#define evt_begin_readable(fd)  epoll_add(fd, EPOLLIN)
#define evt_end_readable(fd)    epoll_remove(fd, EPOLLIN)
#define evt_begin_writable(fd)  epoll_add(fd, EPOLLOUT)
#define evt_end_writable(fd)    epoll_remove(fd, EPOLLOUT)

#define FD_IS_READ_BUSY(t)  (AIO_STATE.d[t].read_ctx.is_busy)
#define FD_IS_WRITE_BUSY(t) (AIO_STATE.d[t].write_ctx.is_busy)

static struct {
#define MAX_CAPACITY        1024000
#define INITIAL_CAPACITY    1024

    aio_state_t   d;
    size_t      c;
} AIO_STATE;


#define READ_COMPLETE   11
#define WRITE_COMPLETE  12
typedef struct st_aio_complete {
    int type;
    union {
        struct st_io_read_ctx   read_ctx;
        struct st_io_write_ctx  write_ctx;
    } complete;
} *aio_complete_t;
typedef struct st_aio_complete item;

#define RING_QUEUE_SIZE 2048
#define RING_QUEUE_MASK (RING_QUEUE_SIZE-1)
typedef struct st_ring_queue {
    item data[RING_QUEUE_SIZE];
    uint16_t head_index;
    uint16_t tail_index;
} *ring_queue;

static  struct st_ring_queue QUEUE_IO_COMPLETE;

static  pthread_t __THIS_THREAD__;

static void     io_init_read_specific_count(int fd, char *buffer, int size_want, READ_CALLBACK cb, size_t expire);
static void     io_init_read_some(int fd, char *buffer, int size_want, READ_CALLBACK cb, size_t expire);
static void     io_init_accept(int fd, ACCEPT_CALLBACK cb, size_t expire);
static void     io_init_write_specific_count(int fd, const char *buffer, int size_want, WRITE_CALLBACK cb, size_t expire);
static void     io_init_connect(int fd, const char *ip, uint16_t port, CONNECT_CALLBACK cb, size_t expire);

static boolean  io_begin_read(int fd);
static boolean  io_sync_read(int fd);
static boolean  io_end_read(int fd);
static boolean  io_begin_write(int fd);
static boolean  io_sync_write(int fd);
static boolean  io_end_write(int fd);
static void     call_io_complete_cb(const item *it);

static void     read_timeout_cb(void *arg);
static void     write_timeout_cb(void *arg);

boolean aio_init() {
    prctl(PR_SET_NAME, "THREAD-IO  ");
    __THIS_THREAD__ = pthread_self();
    __sighandler_t s = signal(SIGPIPE, SIG_IGN);
    if (SIG_ERR == s) {
        LOG(FATAL, "call signal error, errno: %d, desc: %s", errno, strerror(errno));
        return FALSE;
    }

    boolean r = epoll_init();
    if (!r) {
        return FALSE;
    }

    vec_init(&AIO_STATE);

    rq_init(&QUEUE_IO_COMPLETE);


    timing_wheel_module_init();

    sigint_init();
    aio_ext_init();
    return TRUE;
}

boolean aio_in_io_thread() {
    return __THIS_THREAD__ == pthread_self();
}

void aio_free() {
    timing_wheel_module_free();

    sigint_free();
    epoll_free();
    vec_free(&AIO_STATE);
}

void aio_sync() {
    while (TRUE) {
        while (!rq_is_empty(&QUEUE_IO_COMPLETE)) {
            struct st_aio_complete it;
            rq_dequeue(&QUEUE_IO_COMPLETE, &it);
            call_io_complete_cb(&it);
        }

#define MAX_EVENTS 768
        struct epoll_event events[MAX_EVENTS];
        memset(events, 0, sizeof(events));
        int fs = 0;
        boolean r = epoll_poll(events, MAX_EVENTS, 50, &fs);
        if (!r) {
            continue;
        }
        for (size_t i = 0; i < fs; ++i) {
            int readable = events[i].events & EPOLLIN;
            int writeable = events[i].events & EPOLLOUT;
            int fd = events[i].data.fd;

            if (readable) {
                boolean t = io_sync_read(fd);
                if (t) {
                    io_end_read(fd);
                }
            }

            if (writeable) {
                boolean t = io_sync_write(fd);
                if (t) {
                    io_end_write(fd);
                }
            }
        }

        if (0 == fs && sigint_check()) {
            break;
        }

    }
}

const char* aio_err_desc(int err) {
    if (err > 0) {
        return strerror(err);
    } else {
        switch (err) {
            case AIO_ERR_NONE: {
                return "AIO_ERR_NONE";
            };
            case AIO_ERR_FD_FIN: {
                return "AIO_ERR_FD_FIN";
            }
            case AIO_ERR_TIMEOUT: {
                return "AIO_ERR_TIMEOUT";
            }
            case AIO_ERR_USER_ABORT: {
                return "AIO_ERR_USER_ABORT";
            }
            default:{
                return "Unknown AIO ERR";
            }
        }
    }
}

boolean async_read(int fd, int size_to_read, char *dst, READ_CALLBACK cb, size_t expire) {
    if (fd < 0) {
        LOG(FATAL, "call async_read with wrong fd: %d", fd);
        return FALSE;
    }

    if (!vec_check_index(&AIO_STATE, fd)) {
        vec_resize(&AIO_STATE, fd);
    }

    if (FD_IS_READ_BUSY(fd)) {
        LOG(FATAL, "call async_read error, busy now");
        return FALSE;
    }

    io_init_read_specific_count(fd, dst, size_to_read, cb, expire);

    boolean r = io_begin_read(fd);
    if (!r) {
        return FALSE;
    }


    return TRUE;
}

boolean async_read_some(int fd, int size_to_read, char *dst, READ_CALLBACK cb, size_t expire) {
    if (fd < 0) {
        LOG(FATAL, "call async_read_some with wrong fd: %d", fd);
        return FALSE;
    }

    if (!vec_check_index(&AIO_STATE, fd)) {
        vec_resize(&AIO_STATE, fd);
    }

    if (FD_IS_READ_BUSY(fd)) {
        LOG(FATAL, "call async_read_some error, busy now");
        return FALSE;
    }

    io_init_read_some(fd, dst, size_to_read, cb, expire);

    boolean r = io_begin_read(fd);
    if (!r) {
        return FALSE;
    }

    return TRUE;
}

boolean async_accept(int fd, ACCEPT_CALLBACK cb, size_t expire) {
    if (fd < 0) {
        LOG(FATAL, "call async_accept with wrong fd: %d", fd);
        return FALSE;
    }

    if (!vec_check_index(&AIO_STATE, fd)) {
        vec_resize(&AIO_STATE, fd);
    }

    if (FD_IS_READ_BUSY(fd) || FD_IS_WRITE_BUSY(fd)) {
        LOG(FATAL, "call async_accept error, busy now");
        return FALSE;
    }

    io_init_accept(fd, cb, expire);

    boolean r = io_begin_read(fd);
    if (!r) {
        return FALSE;
    }

    return TRUE;
}

boolean async_write(int fd, int size_to_write, const char *src, WRITE_CALLBACK cb, size_t expire) {
    if (fd < 0) {
        LOG(FATAL, "call async_write with wrong fd: %d", fd);
        return FALSE;
    }

    if (!vec_check_index(&AIO_STATE, fd)) {
        vec_resize(&AIO_STATE, fd);
    }

    if (FD_IS_WRITE_BUSY(fd)) {
        LOG(FATAL, "call async_write error, busy now");
        return FALSE;
    }

    io_init_write_specific_count(fd, src, size_to_write, cb, expire);

    boolean r = io_begin_write(fd);
    if (!r) {
        return FALSE;
    }

    r = io_sync_write(fd);
    if (r) {
        io_end_write(fd);
    }

    return TRUE;
}

boolean async_connect(int fd, const char *ip, uint16_t port, CONNECT_CALLBACK cb, size_t expire) {
    if (fd < 0) {
        LOG(FATAL, "call async_connect with wrong fd: %d", fd);
        return FALSE;
    }

    if (!vec_check_index(&AIO_STATE, fd)) {
        vec_resize(&AIO_STATE, fd);
    }

    if (FD_IS_READ_BUSY(fd) || FD_IS_WRITE_BUSY(fd)) {
        LOG(FATAL, "call async_connect error, busy now");
        return FALSE;
    }

    io_init_connect(fd, ip, port, cb, expire);

    boolean r = io_begin_write(fd);
    if (!r) {
        return FALSE;
    }

    r = io_sync_write(fd);
    if (r) {
        io_end_write(fd);
    }

    return TRUE;
}

boolean aio_cancel_read(int fd) {
    return aio_cancel_read_with_err(fd, AIO_ERR_USER_ABORT);
}

boolean aio_cancel_read_with_err(int fd, int err) {
    if (!(vec_check_index(&AIO_STATE, fd))) {
        LOG(FATAL, "call aio_cancel_read with wrong fd: %d", fd);
        return FALSE;
    }

    if (!FD_IS_READ_BUSY(fd)) {
        return TRUE;
    }

    aio_state_t ios = AIO_STATE.d + fd;

    ios->read_ctx.err = err;
    ios->read_ctx.is_busy = FALSE;

    io_end_read(fd);
    return TRUE;
}

boolean aio_cancel_write(int fd) {
    return aio_cancel_write_with_err(fd, AIO_ERR_USER_ABORT);
}

boolean aio_cancel_write_with_err(int fd, int err) {
    if (!(vec_check_index(&AIO_STATE, fd))) {
        LOG(FATAL, "call aio_cancel_write with wrong fd: %d", fd);
        return FALSE;
    }

    if (!FD_IS_WRITE_BUSY(fd)) {
        return TRUE;
    }

    aio_state_t ios = AIO_STATE.d + fd;

    ios->write_ctx.err = err;
    ios->write_ctx.is_busy = FALSE;

    io_end_write(fd);
    return TRUE;
}


void io_init_read_specific_count(int fd,
                                 char *buffer, int size_want, READ_CALLBACK cb, size_t expire) {

    aio_state_t ios = AIO_STATE.d + fd;

    ios->read_ctx.fd        = fd;
    ios->read_ctx.type      = IO_READ_SPECIFIC_COUNT;
    ios->read_ctx.is_busy   = TRUE;
    ios->read_ctx.err       = AIO_ERR_NONE;
    ios->read_ctx.expire    = expire;

    ios->read_ctx.d.read_specific_count.buffer              = buffer;
    ios->read_ctx.d.read_specific_count.size_transferred    = 0;
    ios->read_ctx.d.read_specific_count.size_must           = size_want;
    ios->read_ctx.d.read_specific_count.cb                  = cb;

}

void io_init_read_some(int fd,
                       char *buffer, int size_want, READ_CALLBACK cb, size_t expire) {
    aio_state_t ios = AIO_STATE.d + fd;

    ios->read_ctx.fd        = fd;
    ios->read_ctx.type      = IO_READ_SOME;
    ios->read_ctx.is_busy   = TRUE;
    ios->read_ctx.err       = AIO_ERR_NONE;
    ios->read_ctx.expire    = expire;

    ios->read_ctx.d.read_some.buffer              = buffer;
    ios->read_ctx.d.read_some.size_transferred    = 0;
    ios->read_ctx.d.read_some.size_hope           = size_want;
    ios->read_ctx.d.read_some.cb                  = cb;
}

void io_init_accept(int fd, ACCEPT_CALLBACK cb, size_t expire) {
    aio_state_t ios = AIO_STATE.d + fd;

    ios->read_ctx.fd        = fd;
    ios->read_ctx.type      = IO_ACCEPT;
    ios->read_ctx.is_busy   = TRUE;
    ios->read_ctx.err       = AIO_ERR_NONE;
    ios->read_ctx.expire    = expire;

    ios->read_ctx.d.accept.accepted_fd  = 0;
    ios->read_ctx.d.accept.cb           = cb;
}

void io_init_write_specific_count(int fd, const char *buffer, int size_want, WRITE_CALLBACK cb, size_t expire) {
    aio_state_t ios = AIO_STATE.d + fd;

    ios->write_ctx.fd       = fd;
    ios->write_ctx.type     = IO_WRITE_SPECIFIC_COUNT;
    ios->write_ctx.is_busy  = TRUE;
    ios->write_ctx.err      = AIO_ERR_NONE;
    ios->write_ctx.expire   = expire;

    ios->write_ctx.d.write_specific_count.buffer            = buffer;
    ios->write_ctx.d.write_specific_count.size_transferred  = 0;
    ios->write_ctx.d.write_specific_count.size_must         = size_want;
    ios->write_ctx.d.write_specific_count.cb                = cb;
}

void io_init_connect(int fd, const char *ip, uint16_t port, CONNECT_CALLBACK cb, size_t expire) {
    aio_state_t ios = AIO_STATE.d + fd;

    ios->write_ctx.fd       = fd;
    ios->write_ctx.type     = IO_CONNECT;
    ios->write_ctx.is_busy  = TRUE;
    ios->write_ctx.err      = AIO_ERR_NONE;
    ios->write_ctx.expire   = expire;


    memset(&ios->write_ctx.d.connect.addr, 0 , sizeof ios->write_ctx.d.connect.addr);
    ios->write_ctx.d.connect.addr.sin_family = AF_INET;
    ios->write_ctx.d.connect.addr.sin_port = htons(port);
    inet_pton(AF_INET, ip, &ios->write_ctx.d.connect.addr.sin_addr);
    ios->write_ctx.d.connect.cb = cb;
}

boolean io_begin_read(int fd) {
    aio_state_t ios = AIO_STATE.d + fd;

    if (ios->read_ctx.expire > 0) {
        ios->read_ctx.task = delay_task_ctor(read_timeout_cb, (void*)fd);
        timing_wheel_add(ios->read_ctx.expire, ios->read_ctx.task);
    }

    return evt_begin_readable(fd);
}

boolean io_sync_read(int fd) {
    aio_state_t ios = AIO_STATE.d + fd;
    int         *perr = &(ios->read_ctx.err);
    boolean     *pblocking = &(ios->read_ctx.is_busy);

    switch (ios->read_ctx.type) {
        case IO_READ_SPECIFIC_COUNT: {
            char    *buffer = ios->read_ctx.d.read_specific_count.buffer +
                              ios->read_ctx.d.read_specific_count.size_transferred;
            size_t  want    = ios->read_ctx.d.read_specific_count.size_must -
                              ios->read_ctx.d.read_specific_count.size_transferred;

            ssize_t transferred = read(fd, buffer, want);
            if (transferred > 0) {
                ios->read_ctx.d.read_specific_count.size_transferred += transferred;
                if (transferred == want) {
                    *perr       = AIO_ERR_NONE;
                    *pblocking  = FALSE;
                } else {
                    *pblocking  = TRUE;
                }
            } else if (transferred == 0) {
                *perr       = AIO_ERR_FD_FIN;
                *pblocking  = FALSE;
            } else {
                *perr       = errno;
                *pblocking  = FALSE;
            }
        } break;
        case IO_READ_SOME: {
            char    *buffer = ios->read_ctx.d.read_specific_count.buffer +
                              ios->read_ctx.d.read_specific_count.size_transferred;
            size_t  want    = ios->read_ctx.d.read_specific_count.size_must -
                              ios->read_ctx.d.read_specific_count.size_transferred;

            ssize_t transferred = read(fd, buffer, want);
            if (transferred > 0) {
                ios->read_ctx.d.read_specific_count.size_transferred += transferred;
                *perr = AIO_ERR_NONE;
            } else if (transferred == 0) {
                *perr = AIO_ERR_FD_FIN;
            } else {
                *perr = errno;
            }
            *pblocking  = FALSE;
        } break;
        case IO_ACCEPT: {
            int accepted_fd = accept(fd, NULL, NULL);
            if (-1 != accepted_fd) {
                tcp_socket_enable_nonblock(fd);
                ios->read_ctx.d.accept.accepted_fd = accepted_fd;
                *perr       = AIO_ERR_NONE;
            } else {
                *perr       = errno;
            }
            *pblocking  = FALSE;
        } break;
        default: {
            LOG(FATAL, "unknown io read type: %d", ios->read_ctx.type);
            assert(FALSE);
        }
    }

    return !(*pblocking);
}

boolean io_end_read(int fd) {
    aio_state_t ios = AIO_STATE.d + fd;

    struct st_aio_complete it;
    it.type = READ_COMPLETE;

    it.complete.read_ctx = ios->read_ctx;

    if (rq_is_full(&QUEUE_IO_COMPLETE)) {
        LOG(FATAL, "QUEUE_IO_COMPLETE is full, add fail");
        return FALSE;
    } else {
        rq_queue(&QUEUE_IO_COMPLETE, &it);
    }

    evt_end_readable(fd);

    switch (it.complete.read_ctx.type) {
        case IO_READ_SPECIFIC_COUNT: {
            ios->read_ctx.fd        = fd;
            ios->read_ctx.type      = IO_NONE;
            ios->read_ctx.is_busy   = FALSE;
            ios->read_ctx.err       = AIO_ERR_NONE;

            ios->read_ctx.d.read_specific_count.buffer              = NULL;
            ios->read_ctx.d.read_specific_count.size_transferred    = 0;
            ios->read_ctx.d.read_specific_count.size_must           = 0;
            ios->read_ctx.d.read_specific_count.cb                  = NULL;
        } break;
        case IO_READ_SOME: {
            ios->read_ctx.fd        = fd;
            ios->read_ctx.type      = IO_NONE;
            ios->read_ctx.is_busy   = FALSE;
            ios->read_ctx.err       = AIO_ERR_NONE;

            ios->read_ctx.d.read_some.buffer              = NULL;
            ios->read_ctx.d.read_some.size_transferred    = 0;
            ios->read_ctx.d.read_some.size_hope           = 0;
            ios->read_ctx.d.read_some.cb                  = NULL;
        } break;
        case IO_ACCEPT: {
            ios->read_ctx.fd        = fd;
            ios->read_ctx.type      = IO_NONE;
            ios->read_ctx.is_busy   = FALSE;
            ios->read_ctx.err       = AIO_ERR_NONE;

            ios->read_ctx.d.accept.accepted_fd  = 0;
            ios->read_ctx.d.accept.cb           = NULL;
        } break;
        default: {
            LOG(FATAL, "unknown io read type: %d", ios->read_ctx.type);
            assert(FALSE);
        }
    }

    if (ios->read_ctx.task != NULL) {
        if (delay_task_is_scheduled(ios->read_ctx.task)) {
            timing_wheel_rm(ios->read_ctx.task);
        }
        delay_task_release(ios->read_ctx.task);
        ios->read_ctx.task = NULL;
        ios->read_ctx.expire = 0;
    }

    return TRUE;
}

boolean io_begin_write(int fd) {
    aio_state_t ios = AIO_STATE.d + fd;

    if (ios->write_ctx.expire > 0) {
        ios->write_ctx.task = delay_task_ctor(write_timeout_cb, (void*)fd);
        timing_wheel_add(ios->write_ctx.expire, ios->read_ctx.task);
    }

    return evt_begin_writable(fd);
}

boolean io_sync_write(int fd) {
    aio_state_t ios = AIO_STATE.d + fd;
    int         *perr = &(ios->write_ctx.err);
    boolean     *pblocking = &(ios->write_ctx.is_busy);

    switch (ios->write_ctx.type) {
        case IO_WRITE_SPECIFIC_COUNT: {
            const char  *buffer = ios->write_ctx.d.write_specific_count.buffer +
                                  ios->write_ctx.d.write_specific_count.size_transferred;
            size_t      want = ios->write_ctx.d.write_specific_count.size_must -
                               ios->write_ctx.d.write_specific_count.size_transferred;

            ssize_t transferred = write(fd, buffer, want);
            if (transferred > 0) {
                ios->write_ctx.d.write_specific_count.size_transferred += transferred;
                if (transferred == want) {
                    *perr       = AIO_ERR_NONE;
                    *pblocking  = FALSE;
                } else {
                    *pblocking  = TRUE;
                }
            } else if (-1 == transferred) {
                if (errno == EAGAIN || errno == EWOULDBLOCK) {
                    *pblocking  = TRUE;
                } else {
                    *perr       = errno;
                    *pblocking  = FALSE;
                }
            }
        } break;
        case IO_CONNECT: {
            int t = connect(fd, (const struct sockaddr *)&ios->write_ctx.d.connect.addr,
                            sizeof(ios->write_ctx.d.connect.addr));
            if (0 == t) {
                *perr = AIO_ERR_NONE;
                *pblocking = FALSE;
            } else if (-1 == t && errno == EINPROGRESS) {
                *pblocking = TRUE;
            } else {
                *perr       = errno;
                *pblocking  = FALSE;
            }
        } break;
        default: {
            LOG(FATAL, "unknown io write type: %d", ios->write_ctx.type);
            assert(FALSE);
        }
    }

    return !(*pblocking);
}

boolean io_end_write(int fd) {
    aio_state_t ios = AIO_STATE.d + fd;

    struct st_aio_complete it;
    it.type = WRITE_COMPLETE;

    it.complete.write_ctx = ios->write_ctx;

    if (rq_is_full(&QUEUE_IO_COMPLETE)) {
        LOG(FATAL, "QUEUE_IO_COMPLETE is full, add fail");
        return FALSE;
    } else {
        rq_queue(&QUEUE_IO_COMPLETE, &it);
    }

    evt_end_writable(fd);

    switch (it.complete.write_ctx.type) {
        case IO_WRITE_SPECIFIC_COUNT: {
            ios->write_ctx.fd       = fd;
            ios->write_ctx.type     = IO_NONE;
            ios->write_ctx.is_busy  = FALSE;
            ios->write_ctx.err      = AIO_ERR_NONE;

            ios->write_ctx.d.write_specific_count.buffer            = NULL;
            ios->write_ctx.d.write_specific_count.size_transferred  = 0;
            ios->write_ctx.d.write_specific_count.size_must         = 0;
            ios->write_ctx.d.write_specific_count.cb                = NULL;
        } break;
        case IO_CONNECT: {
            ios->write_ctx.fd = fd;
            ios->write_ctx.type = IO_NONE;
            ios->write_ctx.is_busy = FALSE;
            ios->write_ctx.err = AIO_ERR_NONE;

            memset(&ios->write_ctx.d.connect.addr, 0, sizeof ios->write_ctx.d.connect.addr);
            ios->write_ctx.d.connect.cb = NULL;
        } break;
        default: {
            LOG(FATAL, "unknown io read type: %d", ios->read_ctx.type);
            assert(FALSE);
        }
    }

    if (ios->write_ctx.task != NULL) {
        if (delay_task_is_scheduled(ios->write_ctx.task)) {
            timing_wheel_rm(ios->write_ctx.task);
        }
        delay_task_release(ios->write_ctx.task);
        ios->write_ctx.task = NULL;
        ios->write_ctx.expire = 0;
    }

    return TRUE;
}

void call_io_complete_cb(const item *it) {
    if (it->type == READ_COMPLETE) {
        switch (it->complete.read_ctx.type) {
            case IO_READ_SPECIFIC_COUNT: {
                it->complete.read_ctx.d.read_specific_count.cb(
                        it->complete.read_ctx.fd, it->complete.read_ctx.err,
                        it->complete.read_ctx.d.read_specific_count.buffer,
                        it->complete.read_ctx.d.read_specific_count.size_transferred);
            } break;
            case IO_READ_SOME: {
                it->complete.read_ctx.d.read_some.cb(
                        it->complete.read_ctx.fd, it->complete.read_ctx.err,
                        it->complete.read_ctx.d.read_some.buffer,
                        it->complete.read_ctx.d.read_some.size_transferred);
            } break;
            case IO_READ_UNTIL: {
                it->complete.read_ctx.d.read_until.cb(
                        it->complete.read_ctx.fd, it->complete.read_ctx.err,
                        it->complete.read_ctx.d.read_until.buffer,
                        it->complete.read_ctx.d.read_until.size_transferred);
            } break;
            case IO_ACCEPT: {
                it->complete.read_ctx.d.accept.cb(
                        it->complete.read_ctx.fd, it->complete.read_ctx.err,
                        it->complete.read_ctx.d.accept.accepted_fd);
            } break;
            default: {
                LOG(FATAL, "Unknown io read type: %d", it->complete.read_ctx.type);
            } break;
        }
    } else if (it->type == WRITE_COMPLETE) {
        switch (it->complete.write_ctx.type) {
            case IO_WRITE_SPECIFIC_COUNT: {
                it->complete.write_ctx.d.write_specific_count.cb(
                        it->complete.write_ctx.fd, it->complete.write_ctx.err,
                        it->complete.write_ctx.d.write_specific_count.buffer,
                        it->complete.write_ctx.d.write_specific_count.size_transferred);
            } break;
            case IO_CONNECT: {
                it->complete.write_ctx.d.connect.cb(
                        it->complete.write_ctx.fd, it->complete.write_ctx.err);
            } break;
            default: {
                LOG(FATAL, "Unknown io write type: %d", it->complete.write_ctx.type);
            } break;
        }
    } else {
        LOG(FATAL, "Unknown io complete type: %d", it->type);
    }
}

void read_timeout_cb(void *arg) {
    int fd = (int) arg;
    aio_cancel_read_with_err(fd, AIO_ERR_TIMEOUT);
}

void write_timeout_cb(void *arg) {
    int fd = (int) arg;
    aio_cancel_write_with_err(fd, AIO_ERR_TIMEOUT);
}

int     tcp_socket() {
    return socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
}

boolean tcp_socket_enable_nonblock(int fd) {
    int flags = fcntl(fd, F_GETFL, NULL);
    int r = fcntl(fd, F_SETFL, flags | SOCK_NONBLOCK);
    return -1 != r;
}

boolean socket_bind_listen(int fd, short port) {
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(port);

    int r = bind(fd, (const struct sockaddr *)&addr, sizeof(addr));
    if (-1 == r) {
        LOG(FATAL, "call bind error, errno: %d, desc: %s", errno, strerror(errno));
        return FALSE;
    }
    r = listen(fd, SOMAXCONN);
    if (-1 == r) {
        LOG(FATAL, "call listen error, errno: %d, desc: %s", errno, strerror(errno));
        return FALSE;
    }
    return TRUE;
}
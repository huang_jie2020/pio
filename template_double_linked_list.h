//
// Created by json on 22-12-17.
//

#ifndef AIO_DOUBLE_LINKED_LIST_TEMPLATE_H
#define AIO_DOUBLE_LINKED_LIST_TEMPLATE_H

#define DOUBLE_LINKED_LIST_TEMPLATE_INSTANTIATION(E, T, PTR) \
typedef struct T {                             \
    struct T     *prev;                        \
    struct T     *next;                        \
    E       d;                                 \
} *PTR                                         \

#define DOUBLE_LINKED_LIST_INIT(head) \
do {                                  \
    (head)->next = (head);                \
    (head)->prev = (head);                \
} while (false)                       \

#define DOUBLE_LINKED_LIST_FRONT_INSERT(head, node) \
do {                                                \
    (node)->prev = (head);                          \
    (node)->next = (head)->next;                    \
    (head)->next->prev = (node);                    \
    (head)->next = (node);                          \
} while (false)                                     \

#define DOUBLE_LINKED_LIST_TAIL_INSERT(head, node) \
do {                                               \
    (node)->prev = (head)->prev;                   \
    (node)->next = (head);                         \
    (head)->prev->next = (node);                   \
    (head)->prev = (node);                         \
} while (false)                                    \

#define DOUBLE_LINKED_LIST_REMOVE(head, node) \
do {                                          \
    assert((head) != (node));                 \
    (node)->prev->next = (node)->next;        \
    (node)->next->prev = (node)->prev;        \
    (node)->prev = NULL;                      \
    (node)->next = NULL;                      \
} while (false)                               \

#define DOUBLE_LINKED_LIST_FOR_EACH(head, it, next)     \
for (item = head->next, next = item->next;              \
        item != list; item = next, next = item->next)   \

#endif //AIO_DOUBLE_LINKED_LIST_TEMPLATE_H

//
// Created by json on 22-11-26.
//

#include <stdio.h>
#include <stdlib.h>
#include "template_vector.h"
#include "template_double_linked_list.h"
#include "template_ring_queue.h"
#include "template_priority_queue.h"

typedef struct st_student {
   int age;
   char name[100];
} *student_t;

VECTOR_TEMPLATE_INSTANTIATION(struct st_student, st_vec_student, vec_student_t);

DOUBLE_LINKED_LIST_TEMPLATE_INSTANTIATION(struct st_student, st_dlist_student, dlist_student_t);


PRIORITY_QUEUE_TEMPLATE_INSTANTIATION(struct st_student, st_pqueue_student, pqueue_student_t);



int main() {
    struct st_pqueue_student students;
    PRIORITY_QUEUE_INIT(&students, 102400);

    for (size_t i = 0; i < 1024; ++i) {
        struct st_student student = { 2048-i, "json" };
        PRIORITY_QUEUE_PUSH(&students, &student, student.age);
    }

    struct st_student student;
    while (PRIORITY_QUEUE_SIZE(&students) > 0) {
        int p;
        PRIORITY_QUEUE_POP(&students, &student, &p);
        printf("%s, %d\n", student.name, student.age);
    }


}
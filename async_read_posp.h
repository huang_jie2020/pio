//
// Created by json on 2022/11/16.
//

#ifndef AIO_ASYNC_READ_POSP_REQ_H
#define AIO_ASYNC_READ_POSP_REQ_H

#include "type_def.h"

boolean async_read_posp_req(int fd, char *dst, READ_CALLBACK cb);

boolean async_read_posp(int fd, buffer_t b, POSP_READ_CALLBACK cb);


#endif //AIO_ASYNC_READ_POSP_REQ_H

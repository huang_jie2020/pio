//
// Created by json on 22-12-10.
//


#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <assert.h>

#include "thpool.h"
#include "aio.h"
#include "log.h"

typedef struct st_run_task {
    TASK_ROUTINE run;
    void        *arg;
} *run_task_t;

static  threadpool POOL;
static  int PIPE_FD_READ;

static  int PIPE_FD_WRITE;
static  pthread_mutex_t WRITE_MUTEX;

static  run_task_t TASK;

static void PIPE_READ_CB(int fd, int err, char *dst, size_t size_transferred);

void aio_ext_init() {
    POOL = thpool_init(32);
    pthread_mutex_init(&WRITE_MUTEX, NULL);


    int fd[2];
    int r = pipe(fd);
    if (-1 == r) {
        LOG(FATAL, "call pipe error, %s", strerror(errno));
        return;
    }
    PIPE_FD_READ    = fd[0];
    PIPE_FD_WRITE   = fd[1];
    tcp_socket_enable_nonblock(PIPE_FD_READ);
    TASK = NULL;
    async_read(PIPE_FD_READ, sizeof TASK, (char*)&TASK, PIPE_READ_CB, FALSE);
}

boolean aio_run_task(TASK_ROUTINE run, void *arg) {
    if (aio_in_io_thread()) {
        run(arg);
    } else {
        run_task_t task = malloc(sizeof (*task));
        task->arg = arg;
        task->run = run;
        pthread_mutex_lock(&WRITE_MUTEX);
        write(PIPE_FD_WRITE, &task, sizeof task);
        pthread_mutex_unlock(&WRITE_MUTEX);
    }
    return TRUE;
}

boolean aio_post_task(TASK_ROUTINE run, void *arg) {
    int r = thpool_add_work(POOL, run, arg);
    return 0 == r;
}

static void PIPE_READ_CB(int fd, int err, char *dst, size_t size_transferred) {
    assert(err == AIO_ERR_NONE);
    TASK->run(TASK->arg);
    free(TASK);
    TASK = NULL;
    async_read(PIPE_FD_READ, sizeof TASK, (char*)&TASK, PIPE_READ_CB, FALSE);
}
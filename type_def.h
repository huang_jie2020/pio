//
// Created by json on 2022/11/15.
//

#ifndef AIO_TYPE_DEF_H
#define AIO_TYPE_DEF_H




typedef struct st_buffer *buffer_t;

typedef struct st_delay_task *delay_task_t;

typedef struct st_io_read_ctx {
    // about timeout
    delay_task_t    task;
    size_t          expire;

    // basic info
    int             fd;
    int             type;                   // indicate the io type
    bool            is_busy;                // indicate now is io blocking or not;
    int             err;                    // the result of io

    union {
        struct {
            char            *buffer;
            size_t          size_transferred;
            size_t          size_must;
            READ_CALLBACK   cb;
        } read_specific_count;
        struct {
            char            *buffer;
            size_t          size_transferred;
            size_t          size_hope;
            READ_CALLBACK   cb;
        } read_some;
        struct {
            char            *buffer;
            size_t          size_transferred;
            const char      *target;
            READ_CALLBACK   cb;
        } read_until;
        struct {
            int             accepted_fd;
            ACCEPT_CALLBACK cb;
        } accept;
    } d;

} *io_read_ctx_t;

typedef struct st_io_write_ctx {

    // about timeout
    delay_task_t    task;
    size_t          expire;

    // basic info
    int             fd;
    int             type;
    bool            is_busy;
    int             err;

    union {
        struct {
            const char      *buffer;
            size_t          size_transferred;
            size_t          size_must;
            WRITE_CALLBACK  cb;
        } write_specific_count;
        struct {
            struct sockaddr_in  addr;
            CONNECT_CALLBACK    cb;
        } connect;
    } d;
} *io_write_ctx_t;

typedef struct st_aio_state {
    struct st_io_read_ctx   read_ctx;
    struct st_io_write_ctx  write_ctx;
} *aio_state_t;

typedef void (*HTTP_READ_CALLBACK)(int fd, int err, buffer_t b, size_t size_parsed);
typedef void (*POSP_READ_CALLBACK)(int fd, int err, buffer_t b, size_t size_parsed);

#endif //AIO_TYPE_DEF_H

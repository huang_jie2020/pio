//
// Created by json on 22-11-27.
//

#include "buffer.h"
#include "vector.h"

struct st_buffer {
#define MAX_CAPACITY        10240000
#define INITIAL_CAPACITY    2

    char        *d;
    size_t      c;
    size_t      s;
};

buffer_t buffer_malloc() {
    buffer_t b = malloc(sizeof (struct st_buffer));
    b->s = 0;
    vec_init(b);
    return b;
}

buffer_t buffer_free(buffer_t b) {
    vec_free(b);
    free(b);
}

size_t  buffer_capacity(buffer_t b) {
    return vec_capacity(b);
}

size_t  buffer_size(buffer_t b) {
    return b->s;
}

void    buffer_set_size(buffer_t b, size_t new_size) {
    b->s = new_size;
}

boolean buffer_resize(buffer_t b, size_t i) {
    vec_resize(b, i);
    return TRUE;
}

char *  buffer_begin(buffer_t b) {
    return b->d;
}

char *  buffer_end(buffer_t b) {
    return b->d + b->s;
}
//
// Created by json on 2022/12/19.
//

#ifndef AIO_TEMPLATE_RING_QUEUE_H
#define AIO_TEMPLATE_RING_QUEUE_H

#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>


#define RING_QUEUE_TEMPLATE_INSTANTIATION(E, T, PTR)    \
typedef struct T {                                      \
    size_t c;                                           \
    size_t head_index;                                  \
    size_t tail_index;                                  \
    E      *d;                                          \
} *PTR                                                  \

#define RING_QUEUE_INIT(rq, size)                       \
do {                                                    \
    (rq)->c = (size);                                   \
    (rq)->d = calloc((rq)->c, sizeof(*((rq)->d)));      \
    (rq)->head_index = 0;                               \
    (rq)->tail_index = 0;                               \
} while (false)                                         \

#define RING_QUEUE_FREE(rq, size)       \
do {                                    \
    free((rq)->d);                      \
    (rq)->d = NULL;                     \
    (rq)->c = 0;                        \
    (rq)->head_index = 0;               \
    (rq)->tail_index = 0;               \
} while (false)                         \

#define RING_QUEUE_IS_EMPTY(rq) ((rq)->head_index == (rq)->tail_index)

#define RING_QUEUE_IS_FULL(rq)   ((((rq)->head_index - (rq)->tail_index) & ((rq)->c - 1)) == ((rq)->c - 1))

#define RING_QUEUE_PUSH(rq, pit)                                    \
do {                                                                \
    (rq)->data[(rq)->head_index] = *(pit);                          \
    (rq)->head_index = (((rq)->head_index + 1) & ((rq)->c - 1));    \
} while (false)                                                     \

#define RING_QUEUE_POP(rq, pit)                                     \
do {                                                                \
    *(pit) = (rq)->data[(rq)->tail_index];                          \
    (rq)->tail_index = ((1 + (rq)->tail_index) & ((rq)->c - 1));    \
} while (false)                                                     \




#endif //AIO_TEMPLATE_RING_QUEUE_H

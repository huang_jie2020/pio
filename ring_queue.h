//
// Created by json on 22-11-19.
//

#ifndef AIO_RING_QUEUE_H
#define AIO_RING_QUEUE_H


#define rq_init(rq)                 \
do {                                \
    (rq)->tail_index = 0;             \
    (rq)->head_index = 0;             \
} while(FALSE)                      \

#define rq_is_empty(rq)  ((rq)->head_index == (rq)->tail_index)
#define rq_is_full(rq)   ((((rq)->head_index - (rq)->tail_index) & RING_QUEUE_MASK) == RING_QUEUE_MASK)
#define rq_size(rq)   (((rq)->head_index - (rq)->tail_index) & RING_QUEUE_MASK)

#define rq_queue(rq, it) \
do {                                                            \
    (rq)->data[(rq)->head_index] = *(it);                             \
    (rq)->head_index = (((rq)->head_index + 1) & RING_QUEUE_MASK);  \
} while (FALSE)                                                 \

#define rq_dequeue(rq, pdata) \
do {                                                            \
    *(pdata) = (rq)->data[(rq)->tail_index];                          \
    (rq)->tail_index = ((1 + (rq)->tail_index) & RING_QUEUE_MASK);   \
} while (FALSE)                                                 \

#endif //AIO_RING_QUEUE_H

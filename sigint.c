//
// Created by json on 22-11-29.
//

#include <signal.h>
#include <sys/signalfd.h>
#include <unistd.h>
#include <assert.h>

#include "aio.h"
#include "sigint.h"
#include "log.h"

static __thread int SIGINT_FD = -1;
static __thread struct  signalfd_siginfo info;
static __thread boolean FLAG = FALSE;

static void sigint_cb(int fd, int err, char *dst, size_t size_transferred);

void sigint_init() {
    sigset_t ss;
    sigemptyset(&ss);
    sigaddset(&ss, SIGINT);
    sigprocmask(SIG_BLOCK, &ss, NULL);

    SIGINT_FD = signalfd(-1, &ss, SFD_NONBLOCK);
    async_read(SIGINT_FD, sizeof info, (char*)&info, sigint_cb, FALSE);
}

void sigint_free() {
    close(SIGINT_FD);
}

boolean sigint_check() {
    return FLAG;
}

static void sigint_cb(int fd, int err, char *dst, size_t size_transferred) {
    assert(err == 0);
    FLAG = TRUE;
    LOG(INFO, "SIGINT");
}
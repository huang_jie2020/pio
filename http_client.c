//
// Created by json on 2022/11/29.
//

#include <string.h>

#include "async_read_http.h"
#include "aio.h"
#include "log.h"

#define IP "192.168.1.228"
#define PORT 7080

void connect_cb(int fd, int err);
void write_cb(int fd, int err, const char *src, size_t size_transferred);
void http_read_cb(int fd, int err, buffer_t b, size_t size_transferred);



int main() {
    aio_init();

    int client_fd = tcp_socket();
    tcp_socket_enable_nonblock(client_fd);
    async_connect(client_fd, IP, PORT, connect_cb, TRUE);

    aio_sync();
}

void connect_cb(int fd, int err) {
    if (err) {
        LOG(FATAL, "Connect fail, %s", aio_err_desc(err));
    } else {
        const char *fmt = "POST /ncpay/pos/signIn HTTP/1.1\r\n"
                          "Host: 192.168.1.228:7080\r\n"
                          "Content-Type: application/json\r\n"
                          "Content-Length: %d\r\n"
                          "\r\n"
                          "%s";
        const char *bodyfmt = "{\"termId\": \"%s\",\"termSN\": \"%s\"}";
        char req[1024] = { 0 };
        char body[256] = { 0 };
        sprintf(body, bodyfmt, "999999999999999", "103800");
        sprintf(req, fmt, strlen(body), body);
        async_write(fd, strlen(req), req, write_cb, FALSE);
    }
}

void write_cb(int fd, int err, const char *src, size_t size_transferred) {
    if (err) {
        LOG(FATAL, "Write fail, %s", aio_err_desc(err));
    } else {
        async_read_http(fd, buffer_malloc(), http_read_cb);
    }
}

void http_read_cb(int fd, int err, buffer_t b, size_t size_transferred) {
    if (err) {
        LOG(FATAL, "Read http resp fail");
    } else {
        LOG(INFO, "%s", buffer_begin(b));
    }
}